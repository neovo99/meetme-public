//
//  MainScreenProfileX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 04..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class MainScreenProfileX: UIViewController, FirebaseCurrentUserManager, FirebaseCurrentUserImageManager {
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var labelNameAndAge: UILabel!
    @IBOutlet weak var labelUniversityName: UILabel!
    @IBOutlet weak var labelJobTitle: UILabel!
    @IBOutlet weak var labelBio: UILabel!
    
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        setupProfilePicture()
        
        initDatabaseReference()
        initStorageReference()
        
        if user == nil {
            downloadUserData {
                self.fillLabels()
                self.downloadImages()
            }
            downloadImage(imageName: FirebaseDataStructure.UserImages.profilepicture) { (img) in
                self.user!.profileImage = img
                self.imageViewProfilePicture.image = self.user!.profileImage
            }
        } else {
            self.fillLabels()
            self.imageViewProfilePicture.image = self.user!.profileImage
        }
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func downloadUserData(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            let discoveryData = snapshot.childSnapshot(forPath: FirebaseDataStructure.Users.Discovery.databaseName)
            guard let discoveryValue = discoveryData.value as? NSDictionary else { return }
            
            let uid = currentUser.uid
            let username = value[FirebaseDataStructure.Users.username] as! String
            let bio = value[FirebaseDataStructure.Users.bio] as! String
            let universityName = value[FirebaseDataStructure.Users.universityName] as! String
            let jobtitle = value[FirebaseDataStructure.Users.jobTitle] as! String
            let age = value[FirebaseDataStructure.Users.age] as! Int
            let userGender = value[FirebaseDataStructure.Users.gender] as! String
            
            let minAge = discoveryValue[FirebaseDataStructure.Users.Discovery.minAge] as! Int
            let maxAge = discoveryValue[FirebaseDataStructure.Users.Discovery.maxAge] as! Int
            let discoveryGender = discoveryValue[FirebaseDataStructure.Users.Discovery.gender] as! String
            let discoverySettings = DiscoverySettings(minAge: minAge, maxAge: maxAge, gender: discoveryGender)
            
            let location = Location(long: 0, lat: 0)
            
            var userImages = [UIImage]()
            for _ in FirebaseDataStructure.UserImages.pictures {
                userImages.append(UIImage())
            }
            
            self.user = User(uid: uid, username: username, bio: bio, universityName: universityName, jobTitle: jobtitle, age: age, gender: userGender, discoverySettings: discoverySettings, location: location, profileImage:nil, images:userImages)
            
            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func downloadImage(imageName: String, completion: @escaping (UIImage) -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(currentUser.uid).child(imageName)
        
        userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let img = UIImage(data: data!) {
                    completion(img)
                }
            }
        }
    }
    
    func downloadImages() {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        for (index, imageName) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(currentUser.uid).child(imageName)
            
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        self.user!.images![index] = img
                    }
                }
            }
        }
    }
    
    func uploadImage(imageName: String, image: UIImage) { /* No need for that here */ }
    func deleteImage(imageName: String) { /* No need for that here */ }
    func uploadUserData(user: User) { /* No need for that here */ }
    
    @IBAction func buttonDiscoveryTouched(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.MainScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier:
        Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainProfileScreen.ViewControllers.mainProfileScreenDiscoverySettingsVC) as! MainScreenDiscoverySettingsScreenX
        
        if let user = self.user {
            controller.user = user
        } else {
            print("Error @ Forward from Main to DiscoverySettings: user is nil")
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonEditInfoTouched(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.MainScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainProfileScreen.ViewControllers.mainProfileScreenEditInfoVC) as! MainScreenEditInfoScreenX
        
        if let user = self.user {
            controller.user = user
        } else {
            print("Error @ Forward from Main to EditInfo: user is nil")
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonInformationTouched(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.MainScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainProfileScreen.ViewControllers.MainProfileScreenApplicationInfoVC)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func fillLabels() {
        labelNameAndAge.text = ""
        labelUniversityName.text = ""
        labelJobTitle.text = ""
        labelBio.text = ""
        
        guard let user = user else { return }
        var uniEmpty = false
        var jobEmpty = false
        
        labelNameAndAge.text = ("\(user.username),\(user.age)")
        labelBio.text = user.bio
        
        if user.universityName == "" {
            uniEmpty = true
        }
        if user.jobTitle == "" {
            jobEmpty = true
        }
        if !uniEmpty {
            labelUniversityName.text = user.universityName
            labelJobTitle.text = user.jobTitle
        } else if !jobEmpty {
            labelUniversityName.text = user.jobTitle
        }
    }
    
    func setupProfilePicture() {
        imageViewProfilePicture.roundImage(borderWidth: 1, borderColor: Meetme.Colors.currentUserImageBorder.cgColor)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageViewProfilePicture.isUserInteractionEnabled = true
        imageViewProfilePicture.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.UserProfileScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.UserProfileScreen.userProfileScreenVC) as! UserProfileScreenX
        controller.user = user
        self.present(controller, animated: true, completion: nil)
    }
}
