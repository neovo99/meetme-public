//
//  Utilities.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 18..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

func transitToController(storyboardName:String, viewControllerName:String, completion: @escaping () -> Void) {
    guard let window = UIApplication.shared.keyWindow else { return }
    guard let rootViewController = window.rootViewController else { return }
    let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    let navController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
    navController.view.frame = rootViewController.view.frame
    navController.view.layoutIfNeeded()
    
    UIView.transition(with: window, duration: 0.8, options: .transitionCrossDissolve, animations: {
        window.rootViewController = navController
    }, completion: { (finished) in
        completion()
    })
}

func getCurrentDateString() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyyMMddmmss"
    let dateString = formatter.string(from: date)
    
    return dateString
}

// https://github.com/Orderella/PopupDialog
func popUpDialogDarkMode() {
    let pv = PopupDialogDefaultView.appearance()
    pv.titleFont    = UIFont(name: "HelveticaNeue-Light", size: 16)!
    pv.titleColor   = .white
    pv.messageFont  = UIFont(name: "HelveticaNeue", size: 14)!
    pv.messageColor = UIColor(white: 0.8, alpha: 1)
    

    let pcv = PopupDialogContainerView.appearance()
    pcv.backgroundColor = UIColor(red:0.23, green:0.23, blue:0.27, alpha:1.00)
    pcv.cornerRadius    = 2
    pcv.shadowEnabled   = true
    pcv.shadowColor     = .black
    
    let ov = PopupDialogOverlayView.appearance()
    ov.blurEnabled     = true
    ov.blurRadius      = 30
    ov.liveBlurEnabled = true
    ov.opacity         = 0.7
    ov.color           = .black
    
    let db = DefaultButton.appearance()
    db.titleFont      = UIFont(name: "HelveticaNeue-Medium", size: 14)!
    db.titleColor     = .white
    db.buttonColor    = UIColor(red:0.25, green:0.25, blue:0.29, alpha:1.00)
    db.separatorColor = UIColor(red:0.20, green:0.20, blue:0.25, alpha:1.00)
    
    let cb = CancelButton.appearance()
    cb.titleFont      = UIFont(name: "HelveticaNeue-Medium", size: 14)!
    cb.titleColor     = UIColor(white: 0.6, alpha: 1)
    cb.buttonColor    = UIColor(red:0.25, green:0.25, blue:0.29, alpha:1.00)
    cb.separatorColor = UIColor(red:0.20, green:0.20, blue:0.25, alpha:1.00)
}
