//
//  FirebaseDataStructure.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 27..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation

// Constans
struct FirebaseDataStructure {
    
    struct Users {
        static let databaseName = "users"
        static let uid = "uid"
        static let email = "email"
        static let username = "username"
        static let bio = "bio"
        static let location = "location"
        static let universityName = "universityname"
        static let jobTitle = "jobtitle"
        static let age = "age"
        static let gender = "gender"
        
        struct Discovery {
            static let databaseName = "discovery"
            static let minAge = "minage"
            static let maxAge = "maxage"
            static let gender = "gender"
        }
        
        struct Location {
            static let databaseName = "locations"
            static let longitude = "longitude";
            static let latitude = "latitude";
            static let date = "date"
        }
        
        struct UserLiked {
            static let databaseName = "userliked"
            static let uid = "uid"
            static let date = "date"
        }
        
        struct LikedByOthers {
            static let databaseName = "likedbyothers"
            static let uid = "uid"
            static let date = "date"
        }
        
        struct UserBlacklist {
            static let databaseName = "userblacklist"
            static let uid = "uid"
            static let date = "date"
        }
        
        struct Matches {
            static let databaseName = "matches"
            static let uid = "uid"
        }
    }
    
    struct UserImages {
        static let databaseName = "images"
        static let pictures = ["profilePicture.jpg", "picture2.jpg", "picture3.jpg",
                               "picture4.jpg", "picture5.jpg", "picture6.jpg"]
        static let profilepicture = "profilePicture.jpg"
    }
    
    struct Chat {
        static let databaseName = "chat"
        static let user1Uid = "user1"
        static let user2Uid = "user2"
        
        struct Messages {
            static let databaseName = "messages"
            static let messageId = "messageid"
            static let username = "username"
            static let sender = "sender"
            static let text = "text"
            static let date = "date"
        }
    }
    
}
