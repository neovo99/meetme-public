//
//  LoadingScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 10..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import UICircularProgressRing
import Firebase
import CoreLocation

class LoadingScreenX: UIViewController, UICircularProgressRingDelegate, FirebaseCurrentUserManager, FirebaseCurrentUserImageManager, FirebaseUsersManager {
    @IBOutlet weak var ring: UICircularProgressRingView!
    
    var user: User?
    var users: [User]?
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var isAnimating = true
    
    var isUserDataReady = false
    var isUserImagesReady = false
    var isUsersDataReady = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupRing()
        
        initDatabaseReference()
        initStorageReference()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        downloadUserData {
            self.isUserDataReady = true
            self.downloadImages()
        }
        downloadImage(imageName: FirebaseDataStructure.UserImages.profilepicture) { (img) in
            self.user!.profileImage = img
        }
        downloadUsers {
            if let users = self.users {
                for (index, _) in users.enumerated() {
                    if index == users.endIndex - 1 {
                      self.isUsersDataReady = true
                    }
                }
            }
        }
        loopRingAnimation()
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func downloadUserData(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            let discoveryData = snapshot.childSnapshot(forPath: FirebaseDataStructure.Users.Discovery.databaseName)
            guard let discoveryValue = discoveryData.value as? NSDictionary else { return }
            
            // User data
            let uid = currentUser.uid
            let username = value[FirebaseDataStructure.Users.username] as! String
            let bio = value[FirebaseDataStructure.Users.bio] as! String
            let universityName = value[FirebaseDataStructure.Users.universityName] as! String
            let jobtitle = value[FirebaseDataStructure.Users.jobTitle] as! String
            let age = value[FirebaseDataStructure.Users.age] as! Int
            let userGender = value[FirebaseDataStructure.Users.gender] as! String
            
            //User's Discovery Settings
            let minAge = discoveryValue[FirebaseDataStructure.Users.Discovery.minAge] as! Int
            let maxAge = discoveryValue[FirebaseDataStructure.Users.Discovery.maxAge] as! Int
            let discoveryGender = discoveryValue[FirebaseDataStructure.Users.Discovery.gender] as! String
            let discoverySettings = DiscoverySettings(minAge: minAge, maxAge: maxAge, gender: discoveryGender)
            
            //User's current Location
            let location = Location(long: 0, lat: 0)
            
            var userImages = [UIImage]()
            for _ in FirebaseDataStructure.UserImages.pictures {
                userImages.append(UIImage())
            }
            
            let profilePicture = UIImage()
            
            self.user = User(uid: uid, username: username, bio: bio, universityName: universityName, jobTitle: jobtitle, age: age, gender: userGender, discoverySettings: discoverySettings, location: location, profileImage:profilePicture, images:userImages)
            
            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func downloadImage(imageName: String, completion: @escaping (UIImage) -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(currentUser.uid).child(imageName)
        
        userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let img = UIImage(data: data!) {
                    completion(img)
                }
            }
        }
    }
    
    func downloadImages() {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        for (index, imageName) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(currentUser.uid).child(imageName)
            
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        self.user!.images![index] = img
                    }
                }
                let endIndex = FirebaseDataStructure.UserImages.pictures.count - 1
                if endIndex == index {
                    self.isUserImagesReady = true
                }
            }
        }
    }
    
    func downloadUsers(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        if self.users == nil {
           self.users = [User]()
        }
        
        ref.child(FirebaseDataStructure.Users.databaseName).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            
            while let child = enumerator.nextObject() as? DataSnapshot {
                guard let value = child.value as? NSDictionary else { continue }
                
                let uid = child.key
                
                if uid == currentUser.uid { continue }
                
                // User data
                let username = value[FirebaseDataStructure.Users.username] as! String
                let bio = value[FirebaseDataStructure.Users.bio] as! String
                let universityName = value[FirebaseDataStructure.Users.universityName] as! String
                let jobtitle = value[FirebaseDataStructure.Users.jobTitle] as! String
                let age = value[FirebaseDataStructure.Users.age] as! Int
                let userGender = value[FirebaseDataStructure.Users.gender] as! String
                
                let discoveryData = child.childSnapshot(forPath: FirebaseDataStructure.Users.Discovery.databaseName)
                guard let discoveryValue = discoveryData.value as? NSDictionary else { continue }
                
                // User's Discovery Settings
                let minAge = discoveryValue[FirebaseDataStructure.Users.Discovery.minAge] as! Int
                let maxAge = discoveryValue[FirebaseDataStructure.Users.Discovery.maxAge] as! Int
                let discoveryGender = discoveryValue[FirebaseDataStructure.Users.Discovery.gender] as! String
                let discoverySettings = DiscoverySettings(minAge: minAge, maxAge: maxAge, gender: discoveryGender)
                
                // Get the last valid location
                let location = Location(long: 0, lat: 0)
                
                let locationEnumerator = child.childSnapshot(forPath: FirebaseDataStructure.Users.Location.databaseName).children
                var locations = [Int]()
                while let locationChild = locationEnumerator.nextObject() as? DataSnapshot {
                    guard let key = Int(locationChild.key) else  { continue }
                    locations.append(key)
                }
                locations.sort(by: { $0 > $1})
                
                if let lastNodeName = locations.first {
                    let node = child.childSnapshot(forPath: FirebaseDataStructure.Users.Location.databaseName).childSnapshot(forPath: String(lastNodeName))
                    
                    location.long = node.childSnapshot(forPath: FirebaseDataStructure.Users.Location.longitude).value as! CLLocationDegrees
                    location.lat = node.childSnapshot(forPath: FirebaseDataStructure.Users.Location.latitude).value as! CLLocationDegrees
                }
                
                // User images
                var userImages = [UIImage]()
                var profilePicture = UIImage()
                for _ in FirebaseDataStructure.UserImages.pictures {
                    userImages.append(UIImage())
                }
                
                let userProfileImageRef = self.storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(uid).child(FirebaseDataStructure.UserImages.profilepicture)
                userProfileImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        if let img = UIImage(data: data!) {
                            profilePicture = img
                            print("downloaded profileImage for \(username)")
                            
                            let user = User(uid: uid, username: username, bio: bio, universityName: universityName, jobTitle: jobtitle, age: age, gender: userGender, discoverySettings: discoverySettings, location: location, profileImage:profilePicture, images:userImages)
                            self.users?.append(user)
                            completion()
                        }
                    }
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func downloadUserImages(user: User, completion: @escaping () -> Void) {
        let uid = user.uid
        for (index, item) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = self.storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(uid).child(item)
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        print("\(user.username) downloaded image named: \(item)")
                        user.images![index] = img
                    }
                }
            }
        }
    }
    
    func setupRing() {
        ring.delegate = self
        ring.animationStyle = kCAMediaTimingFunctionLinear
        ring.maxValue = 100
        ring.shouldShowValueText = false
    }
    
    // Looping until the user data is downloaded
    func loopRingAnimation() {
        if !isUserDataReady || !isUserImagesReady || !isUsersDataReady {
            ring.setProgress(to: 100, duration: 3.5, completion: {
                self.ring.setProgress(to: 0, duration: 0, completion: nil)
                self.ring.startAngle += 20
                self.loopRingAnimation()
            })
        } else {
            switchToMainScreen()
        }
    }
    
    func switchToMainScreen() {
        guard let window = UIApplication.shared.keyWindow else { return }
        guard let rootViewController = window.rootViewController else { return }
        
        let storyboard = UIStoryboard(name: Meetme.Storyboards.MainScreen.name, bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.entryNC) as! UINavigationController
        
        let mainProfileScreen = navController.viewControllers.first as! MainScreenX
        mainProfileScreen.user = user
        mainProfileScreen.users = users
        
        navController.view.frame = rootViewController.view.frame
        navController.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.8, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navController
        }, completion: nil)
    }
    
    func uploadUserData(user: User) { /* Protocol method */ }
    func uploadImage(imageName: String, image: UIImage) { /* Protocol method */ }
    func deleteImage(imageName: String) { /* Protocol method */ }
}
