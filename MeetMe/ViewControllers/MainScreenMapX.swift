//
//  MainScreenMapX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 04..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class MainScreenMapX: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, FirebaseCurrentUserManager {
    @IBOutlet weak var mapView: GMSMapView!
    
    var user: User?
    var users: [User]?
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var locationManager = CLLocationManager()
    var currentLocation:CLLocationCoordinate2D?
    var userMarker:CustomGMSMarker?
    var cameraZoom = Float(18) // 10: City, 15: Screets, 20: Buildings
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDatabaseReference()
        initStorageReference()
        
        checkUserBlacklist()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        addStyleToMapView()
        
        userMarker = initUserMarker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startTrackingLocation()
        moveCameraToCurrentPosition()
        updateUserData()
        uploadUserData(user: user!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        startTrackingLocation()
        updateUserData()
        uploadUserData(user: user!)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        guard let userMarker = userMarker else { return }
        currentLocation = locValue
        mapView.clear()
        
        checkUserBlacklist()
        displayUsers()

        userMarker.position = locValue
        userMarker.map = mapView
        
        stopTrackingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let customMarker = marker as? CustomGMSMarker else { return false }
        guard let id = customMarker.id else { return false }
        guard let user = user else { return false }
        guard let users = users else { return false }
        
        let storyboard = UIStoryboard(name: Meetme.Storyboards.UserProfileScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.UserProfileScreen.userProfileScreenVC) as! UserProfileScreenX
        controller.currentUser = self.user
        if id == user.uid {
            controller.user = self.user
            self.present(controller, animated: true, completion: nil)
        } else {
            for item in users {
                if item.uid == id {
                    controller.user = item
                    downloadSelectedUserImages(user: item, completion: { images in
                        controller.user?.images = images
                        self.present(controller, animated: true, completion: nil)
                    })
                    break
                }
            }
        }
        return false
    }
    
    func addStyleToMapView() {
        do {
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func initDatabaseReference() {
        ref = Firebase.Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func downloadSelectedUserImages(user: User, completion: @escaping (_:[UIImage]) -> Void) {
        let uid = user.uid
        var images = [UIImage]()
        var counter = 0
        for _ in FirebaseDataStructure.UserImages.pictures {
            images.append(UIImage())
        }
        
        for (index, item) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = self.storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(uid).child(item)
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    counter += 1
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        images[index] = img
                        counter += 1
                    }
                }
                if counter == FirebaseDataStructure.UserImages.pictures.count {
                    completion(images)
                }
            }
        }
    }
    
    func uploadUserData(user: User) {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let lat = user.location?.lat else { return }
        guard let long = user.location?.long else { return }
        
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.string(from: date)
        
        let newLocationNode = userNode.child(FirebaseDataStructure.Users.Location.databaseName).child(getCurrentDateString())
        newLocationNode.child(FirebaseDataStructure.Users.Location.latitude).setValue(lat)
        newLocationNode.child(FirebaseDataStructure.Users.Location.longitude).setValue(long)
        newLocationNode.child(FirebaseDataStructure.Users.Location.date).setValue(dateString)
    }
    
    func updateUserData() {
        guard let currentLocation = currentLocation else { return }
        guard let user = user else { return }
        let location = Location(long: currentLocation.longitude, lat: currentLocation.latitude)
        user.location = location
    }
    
    func moveCameraToCurrentPosition() {
        guard let currentLocation = currentLocation else { return }
        let camera = GMSCameraPosition.camera(withTarget: currentLocation, zoom: cameraZoom)
        mapView.animate(to: camera)
    }
    
    func startTrackingLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopTrackingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    // Downloads the user's blacklist and delete all the user which is represented there
    func checkUserBlacklist() {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard users != nil else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).child(FirebaseDataStructure.Users.UserBlacklist.databaseName).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            
            while let child = enumerator.nextObject() as? DataSnapshot {
                guard let data = child.value as? NSDictionary else { continue }
                
                let blackListUid = data[FirebaseDataStructure.Users.UserBlacklist.uid] as! String
                
                for (index, user) in self.users!.enumerated() {
                    if user.uid == blackListUid {
                        self.users!.remove(at: index)
                        self.startTrackingLocation()
                        break
                    }
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // Displays users which are met the current user's discovery conditions
    func displayUsers() {
        guard let currentUser = user else { return }
        guard let users = users else { return }
        
        for item in users {
            guard let location = item.location else { continue }
            guard let currentUserDiscoverySettings = currentUser.discoverySettings else { continue }
            if location.lat == 0 { continue }
            if location.long == 0 { continue }
            
            if item.age <= currentUserDiscoverySettings.maxAge &&
                item.age >= currentUserDiscoverySettings.minAge &&
                currentUserDiscoverySettings.gender.contains(item.gender) {
                
                let marker = CustomGMSMarker(position: CLLocationCoordinate2D(latitude: location.lat, longitude: location.long))
                marker.id = item.uid
                marker.userId = item.uid
                
                if let profileImage = item.profileImage {
                    let newImage = imageWithImage(image: profileImage, scaledToSize: CGSize(width: 50.0, height: 50.0))
                    let markerView = UIImageView(image: newImage)
                    markerView.roundImage(borderWidth: 2.0, borderColor: Meetme.Colors.mapMarkerUser.cgColor)
                    
                    marker.iconView = markerView
                    marker.opacity = 0.8
                }
                marker.map = mapView
            }
        }
    }
    
    func initUserMarker() -> CustomGMSMarker {
        guard let user = user else { return CustomGMSMarker() }
        let marker = CustomGMSMarker()
        marker.id = user.uid
        marker.userId = user.uid
        if let profileImage = user.profileImage {
            let newImage = imageWithImage(image: profileImage, scaledToSize: CGSize(width: 50.0, height: 50.0))
            let markerView = UIImageView(image: newImage)
            markerView.roundImage(borderWidth: 2.0, borderColor: Meetme.Colors.mapMarkerCurrentUser.cgColor)
            
            marker.iconView = markerView
            marker.opacity = 0.7
        }
        return marker
    }
    
    // Resizes the image
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func downloadUserData(completion: @escaping () -> Void) {/* No need for that here */}
}
