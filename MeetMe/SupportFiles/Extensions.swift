//
//  Extensions.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 24..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

// Checks if an input's syntax is a valid email address
extension String {
    func isValidEmail() -> Bool {
        // 'try!' will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

// Get previous view controller of the navigation stack
extension UINavigationController {
    
    func previousViewController() -> UIViewController? {
        let lenght = self.viewControllers.count
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
}

extension UIImage {
    // Compare two UIImage object
    func isEqualToImage(image: UIImage) -> Bool {
        guard let data1 =  UIImagePNGRepresentation(self) as NSData? else { return false }
        guard let data2 = UIImagePNGRepresentation(image) as NSData? else { return false }
        return data1 == data2
    }
    
    public var hasContent: Bool {
        return cgImage != nil || ciImage != nil
    }
}

extension UIImageView {
    
    func roundImage(borderWidth: CGFloat ,borderColor: CGColor) {
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = false
        self.layer.borderColor = borderColor
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
    
    func blurImage(intensity: Double) {
        guard let image = self.image else { return }
        let context = CIContext(options: nil)
        let inputImage = CIImage(image: image)
        let originalOrientation = image.imageOrientation
        let originalScale = image.scale
        
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: kCIInputImageKey)
        filter?.setValue(intensity, forKey: kCIInputRadiusKey)
        let outputImage = filter?.outputImage
        
        var cgImage:CGImage?
        
        if let outputImage = outputImage {
            cgImage = context.createCGImage(outputImage, from: (inputImage?.extent)!)
        }
        
        if let cgImageA = cgImage {
            self.image = UIImage(cgImage: cgImageA, scale: originalScale, orientation: originalOrientation)
        }
    }
}

extension Date {
    // Date to String
    func toString( dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
