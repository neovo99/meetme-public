//
//  LIHSlider.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 18..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit

open class LIHSlider: NSObject {
    
    open var sliderImages: [UIImage] = []
    open var sliderDescriptions: [String] = []
    open var descriptionColor: UIColor = UIColor.white
    open var descriptionBackgroundAlpha: CGFloat = 0.3
    open var descriptionBackgroundColor: UIColor = UIColor.black
    open var descriptionFont: UIFont = UIFont.systemFont(ofSize: 15)
    open var numberOfLinesInDescription: Int = 2
    open var transitionInterval: Double = 3.0
    open var customImageView: UIImageView?
    open var showPageIndicator: Bool = true
    open var userInteractionEnabled: Bool = true
    
    //Sliding options
    open var transitionStyle: UIPageViewControllerTransitionStyle = UIPageViewControllerTransitionStyle.scroll
    open var slidingOrientation: UIPageViewControllerNavigationOrientation = UIPageViewControllerNavigationOrientation.horizontal
    open var sliderNavigationDirection: UIPageViewControllerNavigationDirection = UIPageViewControllerNavigationDirection.forward
    
    public init(images: [UIImage]) {
        
        self.sliderImages = images
    }
}
