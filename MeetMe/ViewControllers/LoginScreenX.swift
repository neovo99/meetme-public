//
//  LoginScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 24..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class LoginScreenX: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var labelErrorMessage: UILabel!
    @IBOutlet weak var imageViewBackground: UIImageView!
    
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    var ref:DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        imageViewBackground.blurImage(intensity: 5.0)
        
        initDatabaseReference()
        
        labelErrorMessage.text = ""
        
        activityIndicatorView.center = view.center
        view.addSubview(activityIndicatorView)
        
        textFieldEmail.delegate = self
        textFieldPassword.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            textFieldPassword.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return false
    }
    
    @IBAction func buttonLoginTouched(_ sender: Any) {
        signInUser()
    }
    
    @IBAction func buttonSignUpTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.LoginScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.LoginScreen.ViewControllers.SignUpScreen)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func signInUser() {
        guard let email = textFieldEmail.text else {
            return
        }
        guard let password = textFieldPassword.text else {
            return
        }
        if email == "" {
            labelErrorMessage.text = "The email address is empty"
            return
        }
        if !email.isValidEmail() {
            labelErrorMessage.text = "The email address is malformed"
            return
        }
        if password == "" {
            labelErrorMessage.text = "The password is empty"
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                return
            }
            if let error = error {
                let message = error.localizedDescription
                
                let alert = UIAlertController(title: "Error occured", message: message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            // Checks if it is a newly registered user and sends the user to a specific screen.
            self.handleSignIn(uid: user.uid)
        }
    }
    
    func handleSignIn(uid:String) {
        var isNewUser = false;
        
        activityIndicatorView.startAnimating()
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value  = snapshot.value as? NSDictionary
            if (value?["username"]) != nil {
                isNewUser = false
            } else {
                isNewUser = true
            }
            
            if isNewUser {
                transitToController(storyboardName: Meetme.Storyboards.SetUpProfileScreen.name, viewControllerName: Meetme.Storyboards.SetUpProfileScreen.entryNC, completion: {
                    self.activityIndicatorView.stopAnimating()
                })
            } else {
                transitToController(storyboardName: Meetme.Storyboards.LoadingScreen.name, viewControllerName: Meetme.Storyboards.LoadingScreen.ViewControllers.entry, completion: {
                    self.activityIndicatorView.stopAnimating()
                })
            }
        }) { (error) in
            print(error.localizedDescription)
            if self.activityIndicatorView.isAnimating {
                self.activityIndicatorView.startAnimating()
            }
        }
    }
}
