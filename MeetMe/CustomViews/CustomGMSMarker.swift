//
//  CustomGMSMarker.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 17..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import GoogleMaps

class CustomGMSMarker: GMSMarker {
    /* Identifies the marker */
    var id:String?
    /* Identifies the user who is belonging to */
    var userId:String?
    
}
