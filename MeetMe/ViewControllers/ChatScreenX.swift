//
//  ChatScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 22..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase
import MessageKit

class ChatScreenX: MessagesViewController, MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate, MessageCellDelegate, MessageInputBarDelegate {
    
    var user:User!
    var selectedUser:User!
    var ref:DatabaseReference!
    var storageRef:StorageReference!
    
    var messageList: [CustomMessage] = []
    var conversationNodeRef: DatabaseReference?
    
    var navBar:UINavigationBar!
    var headerView:UIView!
    
    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self
        
        messagesCollectionView.backgroundColor = Meetme.Colors.chatBackground
        
        messageInputBar.backgroundView.backgroundColor = Meetme.Colors.chatBackground
        messageInputBar.inputTextView.backgroundColor = Meetme.Colors.chatInputBar
        messageInputBar.inputTextView.textColor = Meetme.Colors.chatInputBarText
        messageInputBar.inputTextView.placeholderTextColor = Meetme.Colors.chatInputBarPlaceholder
        messageInputBar.sendButton.tintColor = UIColor.red
        
        scrollsToBottomOnKeybordBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false

        setUpHeader()
        
        initDatabaseReference()
        initStorageReference()
        
        downloadMessages(completion: {
            self.attachMessageListener()
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom()
        })
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        messagesCollectionView.contentInset.top = CGFloat(headerView.frame.height)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 100
    }
    
    /* MessageDataSource */
    func currentSender() -> Sender {
        return Sender(id: user!.uid, displayName: user!.username)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            //return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedStringKey.foregroundColor: Meetme.Colors.chatDateIndicatorText])
        }
        return nil
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
    /* MessageDisplayDelegate */
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .darkText
    }
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedStringKey: Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .url]
    }
    
    // MARK: - All Messages
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? Meetme.Colors.chatBubbleCurrentUser : UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
        //        let configurationClosure = { (view: MessageContainerView) in}
        //        return .custom(configurationClosure)
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        var avatar = Avatar()
        
        if message.sender.id == user.uid{
            avatar = Avatar(image: user.profileImage, initials: user.username)
        } else {
            avatar = Avatar(image: selectedUser.profileImage, initials: selectedUser.username)
        }
        avatarView.set(avatar: avatar)
    }
    
    /* MessageLayoutDelegate */
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if indexPath.section % 3 == 0 {
            return 10
        }
        return 0
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
    /* MessageCellDelegate */
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        print("Top cell label tapped")
    }
    
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        print("Top message label tapped")
    }
    
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }
    
    /* MesageLabelDelegate */
    
    func didSelectAddress(_ addressComponents: [String: String]) {
        print("Address Selected: \(addressComponents)")
    }
    
    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }
    
    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }
    
    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }
    
    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        print("TransitInformation Selected: \(transitInformation)")
    }
    
    /* MessageInputBarDelegate */
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        // Each NSTextAttachment that contains an image will count as one empty character in the text: String
        
        for component in inputBar.inputTextView.components {
            
            if let text = component as? String {
                
                let attributedText = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 15), .foregroundColor: UIColor.blue])
                uploadMessage(attributedText: attributedText, sender: currentSender(), date: Date(), data: MessageData.text(text), text: text) { (message) in
                    self.messageList.append(message)
                    self.messagesCollectionView.insertSections([self.messageList.count - 1])
                }
            }
        }
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom()
    }
    
    /* Functions */
    
    func setUpHeader() {
        let viewWidth = self.view.frame.size.width
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: 75))
        
        let imgViewDiameter:CGFloat = 35
        let imgViewCenterX = (viewWidth / CGFloat(2)) - (imgViewDiameter / CGFloat(2))
        
        let imgView = UIImageView(frame: CGRect(x: imgViewCenterX, y: 20, width: imgViewDiameter, height: imgViewDiameter))
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgView.image = selectedUser.profileImage
        imgView.roundImage(borderWidth: 1, borderColor: Meetme.Colors.chatHeadBorder.cgColor)
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(tapGestureRecognizer)
        
        let descLabel = UILabel(frame: CGRect(x: 0, y: 30, width: headerView.frame.size.width , height: headerView.frame.size.height - 10))
        descLabel.text = selectedUser.username
        descLabel.textAlignment = .center
        descLabel.textColor = Meetme.Colors.chatHeaderText
        
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 20, width: 50, height: 50)
        backButton.setImage(UIImage(named: "backArrowLeftWhite"), for: .normal)
        backButton.addTarget(self, action: #selector(ChatScreenX.handleBackButton), for: .touchUpInside)
        
        headerView.backgroundColor = Meetme.Colors.chatHeader
        headerView.addSubview(imgView)
        headerView.addSubview(descLabel)
        headerView.addSubview(backButton)
        self.view.addSubview(headerView)
    }
    
    @objc func handleBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.UserProfileScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.UserProfileScreen.userProfileScreenVC) as! UserProfileScreenX
        controller.currentUser = user
        controller.user = selectedUser
        downloadSelectedUserImages(user: selectedUser, completion: { images in
            controller.user?.images = images
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func initConversationNodeReference() {
        // Runs if there was no conversation before this point.
        ref.child(FirebaseDataStructure.Chat.databaseName).observeSingleEvent(of: .value, with: { (snapshot) in
            let chatEnumerator = snapshot.children
            
            while let chat = chatEnumerator.nextObject() as? DataSnapshot {
                guard let data = chat.value as? NSDictionary else { continue }
                
                let user1 = data[FirebaseDataStructure.Chat.user1Uid] as! String
                let user2 = data[FirebaseDataStructure.Chat.user2Uid] as! String
                
                if self.selectedUser.uid != user1 && self.selectedUser.uid != user2 {
                    continue
                }
                if self.user.uid != user1 && self.user.uid != user2 {
                    continue
                }
                let messageNodeReference = self.ref.child(FirebaseDataStructure.Chat.databaseName).child(chat.key)
                self.conversationNodeRef = messageNodeReference
            }
        }) { (error) in
            print(error.localizedDescription)
        }
            
        if conversationNodeRef == nil {
            conversationNodeRef = ref.child(FirebaseDataStructure.Chat.databaseName).childByAutoId()
            conversationNodeRef?.child(FirebaseDataStructure.Chat.user1Uid).setValue(user.uid)
            conversationNodeRef?.child(FirebaseDataStructure.Chat.user2Uid).setValue(selectedUser.uid)
        }
    }
    
    func attachMessageListener() {
        if conversationNodeRef == nil {
            initConversationNodeReference()
        }
        
        let messagesNode = conversationNodeRef?.child(FirebaseDataStructure.Chat.Messages.databaseName)
        
        messagesNode?.queryLimited(toLast: 1).observe(.value, with: { (snapshot) in
            
            let nodeEnumerator = snapshot.children
            while let child = nodeEnumerator.nextObject() as? DataSnapshot {
                guard let data = child.value as? NSDictionary else { continue }
                guard let messageId = data[FirebaseDataStructure.Chat.Messages.messageId] as? String else { continue }
                
                var isNotDispayed = true
                
                for message in self.messageList {
                    if message.messageId == messageId {
                        isNotDispayed = false
                    }
                }
                
                if isNotDispayed {
                    guard let senderUid = data[FirebaseDataStructure.Chat.Messages.sender] as? String else { return }
                    guard let text = data[FirebaseDataStructure.Chat.Messages.text] as? String else { return }
                    guard let dateString = data[FirebaseDataStructure.Chat.Messages.date] as? String else { return }
                    guard let username = data[FirebaseDataStructure.Chat.Messages.username] as? String else { return }
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyyMMddHHmmss"
                    var date = Date()
                    if let convertedDate = dateFormatter.date(from: dateString) {
                        date = convertedDate
                    }
                    
                    let sender = Sender(id: senderUid, displayName: username)
                    let customMessage = CustomMessage(text: text, sender: sender, messageId: messageId, date: date, data: MessageData.text(text))
                    
                    self.messageList.append(customMessage)
                    self.messagesCollectionView.reloadData()
                    self.messagesCollectionView.scrollToBottom()
                }
            }
        }, withCancel: { (error) in
            print(error.localizedDescription)
        })
    }
    
    func uploadMessage(attributedText: NSAttributedString, sender: Sender, date: Date, data: MessageData, text: String, completion: @escaping (_:CustomMessage) -> Void) {
        if conversationNodeRef == nil {
            initConversationNodeReference()
        }
        let messagesNode = conversationNodeRef!.child(FirebaseDataStructure.Chat.Messages.databaseName)
        let newMessageNode = messagesNode.childByAutoId()
        let convertedDate = date.toString(dateFormat: "yyyyMMddHHmmss")
        newMessageNode.child(FirebaseDataStructure.Chat.Messages.date).setValue(convertedDate)
        newMessageNode.child(FirebaseDataStructure.Chat.Messages.messageId).setValue(newMessageNode.key)
        newMessageNode.child(FirebaseDataStructure.Chat.Messages.sender).setValue(sender.id)
        newMessageNode.child(FirebaseDataStructure.Chat.Messages.text).setValue(text)
        newMessageNode.child(FirebaseDataStructure.Chat.Messages.username).setValue(sender.displayName)
        
        let message = CustomMessage(attributedText: attributedText, sender: sender, messageId: newMessageNode.key, date: Date(), data: MessageData.text(text))
        
        completion(message)
    }
    
    func downloadSelectedUserImages(user: User, completion: @escaping (_:[UIImage]) -> Void) {
        let uid = user.uid
        var images = [UIImage]()
        var counter = 0
        for _ in FirebaseDataStructure.UserImages.pictures {
            images.append(UIImage())
        }
        
        for (index, item) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = self.storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(uid).child(item)
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    counter += 1
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        images[index] = img
                        counter += 1
                    }
                }
                if counter == FirebaseDataStructure.UserImages.pictures.count {
                    completion(images)
                }
            }
        }
    }
    
    func downloadMessages(completion: @escaping () -> Void) {
        ref.child(FirebaseDataStructure.Chat.databaseName).observeSingleEvent(of: .value, with: { (snapshot) in

            let chatEnumerator = snapshot.children
            
            while let chat = chatEnumerator.nextObject() as? DataSnapshot {
                guard let data = chat.value as? NSDictionary else { continue }

                let user1 = data[FirebaseDataStructure.Chat.user1Uid] as! String
                let user2 = data[FirebaseDataStructure.Chat.user2Uid] as! String
            
                if self.selectedUser.uid != user1 && self.selectedUser.uid != user2 {
                    continue
                }
                if self.user.uid != user1 && self.user.uid != user2 {
                    continue
                }
                
                // Create reference for easy upload
                let messageNodeReference = self.ref.child(FirebaseDataStructure.Chat.databaseName).child(chat.key)
                self.conversationNodeRef = messageNodeReference

                // Message download
                let messageNode = chat.childSnapshot(forPath: FirebaseDataStructure.Chat.Messages.databaseName)
                let messagesEnumerator = messageNode.children
                
                while let message = messagesEnumerator.nextObject() as? DataSnapshot {
                    guard let data = message.value as? NSDictionary else { continue }
                    
                    let messageId = data[FirebaseDataStructure.Chat.Messages.messageId] as! String
                    let senderUid = data[FirebaseDataStructure.Chat.Messages.sender] as! String
                    let text = data[FirebaseDataStructure.Chat.Messages.text] as! String
                    let dateString = data[FirebaseDataStructure.Chat.Messages.date] as! String
                    let username = data[FirebaseDataStructure.Chat.Messages.username] as! String
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyyMMddHHmmss"
                    var date = Date()
                    if let convertedDate = dateFormatter.date(from: dateString) {
                        date = convertedDate
                    }
                    
                    let sender = Sender(id: senderUid, displayName: username)
                    let customMessage = CustomMessage(text: text, sender: sender, messageId: messageId, date: date, data: MessageData.text(text))
                    
                    self.messageList.append(customMessage)
                }
            }
            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
