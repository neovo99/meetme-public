//
//  FirebaseManager.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 05..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol FirebaseDataManager {
    var ref: DatabaseReference! { get set }
    func initDatabaseReference()
}

protocol FirebaseCurrentUserManager : FirebaseDataManager {
    var user:User? { get set }
    func downloadUserData(completion: @escaping () -> Void)
    func uploadUserData(user:User)
}

protocol FirebaseCurrentUserImageManager {
    var storageRef: StorageReference! { get set }
    func initStorageReference()
    func downloadImage(imageName: String, completion: @escaping (UIImage) -> Void)
    func downloadImages()
    func uploadImage(imageName:String, image:UIImage)
    func deleteImage(imageName:String)
}

protocol FirebaseUsersManager : FirebaseDataManager {
    var users:[User]? { get set }
    func downloadUsers(completion: @escaping () -> Void)
    func downloadUserImages(user: User, completion: @escaping () -> Void)
}
