//
//  MainScreenEditInfoScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 05..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class MainScreenEditInfoScreenX: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate, FirebaseCurrentUserManager, FirebaseCurrentUserImageManager {
    @IBOutlet weak var textViewBio: UITextView!
    @IBOutlet weak var textFieldUniversity: UITextField!
    @IBOutlet weak var textFieldJobTitle: UITextField!
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var imageViewPicture2: UIImageView!
    @IBOutlet weak var imageViewPicture3: UIImageView!
    @IBOutlet weak var imageViewPicture4: UIImageView!
    @IBOutlet weak var imageViewPicture5: UIImageView!
    @IBOutlet weak var imageViewPicture6: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var user: User?
    var imageViews:[UIImageView]!
    var tappedImageTag:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        imageViews = [imageViewProfilePicture, imageViewPicture2, imageViewPicture3,
                  imageViewPicture4, imageViewPicture5, imageViewPicture6]
        
        textFieldJobTitle.delegate = self
        textFieldUniversity.delegate = self
        textViewBio.delegate = self
        
        initDatabaseReference()
        initStorageReference()
        
        if user == nil {
            downloadUserData(completion: {})
            downloadImages()
        } else {
            setupView(user: user!)
        }
        
        textFieldUniversity.layer.borderWidth = 1.0
        textFieldJobTitle.layer.borderWidth = 1.0
        textFieldUniversity.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
        textFieldJobTitle.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0...1:
            scrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
        default: break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0{
            textFieldJobTitle.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let theInfo = info as NSDictionary
        let img = theInfo.object(forKey: UIImagePickerControllerOriginalImage) as! UIImage
        
        for imageView in imageViews {
            guard let tag = self.tappedImageTag else { return }
            if imageView.tag == tag {
                imageView.image = img
                addDeleteButton(to: imageView)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func downloadUserData(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let data = snapshot.value as? NSDictionary else { return }
            
            let bio = data[FirebaseDataStructure.Users.bio] as! String
            let universityName = data[FirebaseDataStructure.Users.universityName] as! String
            let jobTitle = data[FirebaseDataStructure.Users.jobTitle] as! String
            
            self.textViewBio.text = bio
            self.textFieldUniversity.text = universityName
            self.textFieldJobTitle.text = jobTitle

            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func uploadUserData(user: User) {
        guard let currentUser = Auth.auth().currentUser else { return }
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)
        
        let bio = textViewBio.text
        let universityName = textFieldUniversity.text
        let jobTitle = textFieldJobTitle.text
        userNode.child(FirebaseDataStructure.Users.bio).setValue(bio)
        userNode.child(FirebaseDataStructure.Users.universityName).setValue(universityName)
        userNode.child(FirebaseDataStructure.Users.jobTitle).setValue(jobTitle)
    }

    func downloadImage(imageName: String, completion: @escaping (UIImage) -> Void) { }
    
    func downloadImages(){
        guard let currentUser = Auth.auth().currentUser else { return }
        
        for (index, imageName) in FirebaseDataStructure.UserImages.pictures.enumerated() {
            let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(currentUser.uid).child(imageName)
            
            userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if let img = UIImage(data: data!) {
                        self.imageViews[index].image = img
                        self.user!.images![index] = img
                    }
                }
            }
        }
    }
    
    func uploadImage(imageName: String, image: UIImage) {
        guard let user = Auth.auth().currentUser else { return }
        let userImageRef = storageRef.child("\(FirebaseDataStructure.UserImages.databaseName)").child(user.uid).child(imageName)
        
        if let imageData = UIImageJPEGRepresentation(image, 0.5) {
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            let uploadTask = userImageRef.putData(imageData, metadata: metadata, completion: { (metadata, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            })
            uploadTask.observe(.progress) { (snapshot) in
                guard let progress = snapshot.progress else { return }
                if progress.completedUnitCount == progress.totalUnitCount {
                }
            }
        }
    }
    
    func deleteImage(imageName: String) {
        guard let user = Auth.auth().currentUser else {
            return
        }
        let userImageRef = storageRef.child("\(FirebaseDataStructure.UserImages.databaseName)").child(user.uid).child(imageName)
        
        userImageRef.delete { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func setupView(user:User) {
        self.textViewBio.text = user.bio
        self.textFieldUniversity.text = user.universityName
        self.textFieldJobTitle.text = user.jobTitle
        
        if let userImages = user.images {
            for (index, imageView) in imageViews.enumerated() {
                if let image = userImages[index] {
                    imageView.image = image
                    addTapGestureRecognizer(to:imageView)
                    addDeleteButton(to:imageView)
                }
            }
        } else {
            downloadImages()
        }
    }
    
    func addTapGestureRecognizer(to imageView:UIImageView) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.tappedImageTag = tappedImage.tag
        
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(controller, animated: true, completion: nil)
    }
    
    func addDeleteButton(to imageView:UIImageView) {
        guard let image = imageView.image else { return }
        
        let data = UIImageJPEGRepresentation(image, 1)
        
        guard (data?.count) != nil else { return }
        
        let width = CGFloat(20)
        let height = CGFloat(20)
        let x = imageView.bounds.maxX - width + 5
        let y = imageView.bounds.minY - 5
        let button = UIButton(frame: CGRect(x: x, y: y, width: width, height: height))
        let buttonIcon = UIImage(named: "iconDelete")
        button.setImage(buttonIcon, for: .normal)
        button.tag = imageView.tag
        button.addTarget(self, action: #selector(buttonDeleteTapped(sender:)), for: .touchUpInside)
        
        imageView.addSubview(button)
    }
    
    @objc func buttonDeleteTapped(sender: UIButton) {
        for imageView in imageViews {
            if imageView.tag == sender.tag {
                let imageName = FirebaseDataStructure.UserImages.pictures[(imageView.tag - 1)]
                
                imageView.image = UIImage()
                deleteImage(imageName: imageName)
                
                sender.removeFromSuperview()
            }
        }
    }
    @IBAction func buttonBackTouched(_ sender: UIButton) {
        let controller = navigationController?.previousViewController() as! MainScreenProfileX
        
        if let user = user {
            uploadUserData(user: user)
            user.bio = textViewBio.text
            user.universityName = textFieldUniversity.text
            user.jobTitle = textFieldJobTitle.text
            
            for (index, imageView) in self.imageViews.enumerated() {
                uploadImage(imageName: FirebaseDataStructure.UserImages.pictures[index], image: imageView.image!)
                user.images![index] = imageView.image!
            }
            
            if user.bio != controller.labelBio.text! {
                controller.labelBio.text = user.bio
            }
            if user.universityName != controller.labelUniversityName.text! {
                controller.labelUniversityName.text = user.universityName
            }
            if user.jobTitle != controller.labelJobTitle.text {
                controller.labelJobTitle.text = user.jobTitle
            }
            if let profilePicture = user.images![0] {
                if let currentProfilePicture = controller.imageViewProfilePicture.image {
                    if !profilePicture.isEqualToImage(image: currentProfilePicture) {
                        controller.imageViewProfilePicture.image = profilePicture
                    }
                }
            }
            controller.user = user
        } else {
            print("Error @ MainDiscoverySettings: Cannot upload user data because it's nil")
        }
        navigationController?.popToViewController(controller, animated: true)
    }
}
