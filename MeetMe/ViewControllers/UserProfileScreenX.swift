//
//  UserProfileScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 17..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import PopupDialog

class UserProfileScreenX: UIViewController, LIHSliderDelegate {
    
    @IBOutlet weak var labelUserNameAndAge: UILabel!
    @IBOutlet weak var labelUserUniversity: UILabel!
    @IBOutlet weak var labelUserJobTitle: UILabel!
    @IBOutlet weak var labelUserDistance: UILabel!
    @IBOutlet weak var labelUserBio: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var buttonDislike: UIButton!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var imagePinIcon: UIImageView!
    
    var ref: DatabaseReference!
    var user:User?
    var currentUser:User?
    var userImages = [UIImage]()
    var sliderVC: LIHSliderViewController!
    var isSelectedUserLiked = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDatabaseReference()
        
        setupView()
        setupImageSlider()
        if userCheck() {
            self.hideLikeButtons()
            labelUserDistance.text = ""
            imagePinIcon.isHidden = true
        } else {
            let distanceInKms = calculateDistance()
            if distanceInKms < 1 {
                labelUserDistance.text = "Less than 1 km away"
            } else {
                labelUserDistance.text = "More \(distanceInKms) kms away"
            }
        }
        likedCheck()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLayoutSubviews() {
        self.sliderVC!.view.frame = self.sliderContainer.frame
        mainView.bringSubview(toFront: backButton)
    }
    
    func itemPressedAtIndex(index: Int) {
        print("index \(index) is pressed")
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func setupView() {
        if let user = user {
            labelUserNameAndAge.text = ("\(user.username), \(user.age)")
            
            if let universityName = user.universityName {
                labelUserUniversity.text = universityName
            }
            if let jobTitle = user.jobTitle {
                labelUserJobTitle.text = jobTitle
            }
            if let bio = user.bio {
                labelUserBio.text = bio
            }
            if let images = user.images {
                for image in images {
                    guard let image = image else { continue }
                    if image.hasContent {
                        self.userImages.append(image)
                    }
                }
            }
        }
    }
    
    func setupImageSlider() {
        let slider1: LIHSlider = LIHSlider(images: userImages)
        self.sliderVC  = LIHSliderViewController(slider: slider1)
        sliderVC.delegate = self
    
        self.addChildViewController(self.sliderVC)
        mainView.addSubview(self.sliderVC.view)
        self.sliderVC.didMove(toParentViewController: self)
    }
    
    func hideLikeButtons() {
        buttonDislike.isHidden = true
        buttonLike.isHidden = true
    }
    
    func hideButton(button: UIButton) {
        button.isHidden = true
    }
    
    // Identifies the user
    // If the user = logged in user, then hide buttonLike and buttonDislike
    // Returns true if user = logged in user
    func userCheck() -> Bool {
        guard let loggedInUser = Auth.auth().currentUser else { return false }
        guard let user = user else { return false }
        
        if loggedInUser.uid == user.uid {
            return true
        }
        return false
    }
    
    // Checks if the current user has already liked the selected user
    // if current user has liked then hide buttonLike and buttonDislike
    // and display a visual cue for that scenario
    func likedCheck() {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let selectedUser = user else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).child(FirebaseDataStructure.Users.UserLiked.databaseName).observeSingleEvent(of: .value, with:  { (snapshot) in
            
            let enumerator = snapshot.children
            
            while let child = enumerator.nextObject() as? DataSnapshot {
                guard let data = child.value as? NSDictionary else { continue }
                
                let likedUserUid = data[FirebaseDataStructure.Users.UserLiked.uid] as! String
                
                if selectedUser.uid == likedUserUid {
                    self.isSelectedUserLiked = true
                    self.hideButton(button: self.buttonDislike)
                    self.animate(button: self.buttonLike)
                    break
                }
            }
        }) { (error) in
                print(error.localizedDescription)
            }
    }
    
    // Checks if the selected user has also liked the current user
    func matchCheck() {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let selectedUser = user else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).child(FirebaseDataStructure.Users.LikedByOthers.databaseName).observeSingleEvent(of: .value, with:  { (snapshot) in
            
            let enumerator = snapshot.children
            
            while let child = enumerator.nextObject() as? DataSnapshot {
                guard let data = child.value as? NSDictionary else { continue }
                
                let likedByUser = data[FirebaseDataStructure.Users.LikedByOthers.uid] as! String
                
                if selectedUser.uid == likedByUser {
                    self.storeMatch()
                    self.displayPopDialog()
                    break
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // Stores matches in Firebase
    func storeMatch() {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let selectedUser = user else { return }
        
        let user1Node = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)
        let user2Node = ref.child(FirebaseDataStructure.Users.databaseName).child(selectedUser.uid)
        
        let user1MatchesNode = user1Node.child(FirebaseDataStructure.Users.Matches.databaseName)
        let user2MatchesNode = user2Node.child(FirebaseDataStructure.Users.Matches.databaseName)
        
        user1MatchesNode.child(selectedUser.uid).setValue(selectedUser.uid)
        user2MatchesNode.child(currentUser.uid).setValue(currentUser.uid)
    }

    func animate(button: UIButton) {
        UIView.animate(withDuration: 1, animations: {
            button.frame.size.width += 10
            button.frame.size.height += 10
            button.frame.origin.x -= 50
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat] ,  animations: {
                button.frame.origin.y -= 20
            })
        })
    }
    
    func animatedHide(button: UIButton) {
        UIView.animate(withDuration: 1) {
            button.alpha = 0
        }
    }
    
    func displayPopDialog() {
        popUpDialogDarkMode()
        let title = "Congratulations!"
        let message = "It's a Match"
        let image = UIImage(named: "match")
        
        let popup = PopupDialog(title: title, message: message, image: image, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: CGFloat(320), gestureDismissal: true, hideStatusBar: true) {
            self.dismiss(animated: true, completion: nil)
        }
        let buttonOne = CancelButton(title: "OKAY", action: nil)
        popup.addButton(buttonOne)

        self.present(popup, animated: true, completion: nil)
    }
    
    // Returns result in kilometres
    func calculateDistance() -> Int {
        guard let currentUserLocation = currentUser?.location else { return Int() }
        guard let userLocation = user?.location else { return Int() }
        
        let coordinate1 = CLLocation(latitude: currentUserLocation.lat, longitude: currentUserLocation.long)
        let coordinate2 = CLLocation(latitude: userLocation.lat, longitude: userLocation.long)
        
        let distanceInKms = Int(coordinate1.distance(from: coordinate2) / 1000)
        
        return distanceInKms
    }

    @IBAction func buttonBackTouched(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonDislikeTouched(_ sender: UIButton) {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let selectedUser = user else { return }
        
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)
        let userLikedNode = userNode.child(FirebaseDataStructure.Users.UserBlacklist.databaseName)
        let newNode = userLikedNode.child(selectedUser.uid)
        newNode.child(FirebaseDataStructure.Users.UserLiked.uid).setValue(selectedUser.uid)
        newNode.child(FirebaseDataStructure.Users.UserLiked.date).setValue(getCurrentDateString())
    
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonLikeTouched(_ sender: UIButton) {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let selectedUser = user else { return }
        if isSelectedUserLiked { return }
        
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)
        let userLikedNode = userNode.child(FirebaseDataStructure.Users.UserLiked.databaseName)
        let newUserLikedNode = userLikedNode.child(selectedUser.uid)
        newUserLikedNode.child(FirebaseDataStructure.Users.UserLiked.uid).setValue(selectedUser.uid)
        newUserLikedNode.child(FirebaseDataStructure.Users.UserLiked.date).setValue(getCurrentDateString())
        
        let selectedUserNode = ref.child(FirebaseDataStructure.Users.databaseName).child(selectedUser.uid)
        let selectedUserLikedByOthersNode = selectedUserNode.child(FirebaseDataStructure.Users.LikedByOthers.databaseName)
        let newSelectedUserLikedByOthersNode = selectedUserLikedByOthersNode.child(currentUser.uid)
        newSelectedUserLikedByOthersNode.child(FirebaseDataStructure.Users.LikedByOthers.uid).setValue(currentUser.uid)
        newSelectedUserLikedByOthersNode.child(FirebaseDataStructure.Users.LikedByOthers.date).setValue(getCurrentDateString())
        
        isSelectedUserLiked = true
        animate(button: buttonLike)
        animatedHide(button: buttonDislike)
        
        matchCheck()
    }
}
