//
//  SetUpProfileWelcomeScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 25..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit

class SetUpProfileWelcomeScreenX: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func buttonNextTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: Meetme.Storyboards.SetUpProfileScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.SetUpProfileScreen.ViewControllers.SetUpProfileScreenVC)
        navigationController?.pushViewController(controller, animated: true)
    }
}
