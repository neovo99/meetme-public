//
//  MainScreenDiscoveryX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 09..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class MainScreenDiscoveryX: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var user:User?
    var users:[User]?
    var ref: DatabaseReference!
    var matches = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        initDatabaseReference()
        
        downloadMatches(completion: {
            self.tableView.reloadData()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        downloadMatches(completion: {
            self.tableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        let cellLabelText = matches[indexPath.row].username
        
        guard let cellImage = matches[indexPath.row].profileImage else {
            cell.setupCell(image: nil, labelText: cellLabelText)
            return cell
        }
        
        cell.setupCell(image: cellImage, labelText: cellLabelText)
        cell.imgView.roundImage(borderWidth: 1, borderColor: Meetme.Colors.chatHeadBorder.cgColor)
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = Meetme.Colors.tableViewCellSelected
        cell.selectedBackgroundView = backgroundView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = user else { return }
        
        let controller = ChatScreenX()
        controller.user = user
        controller.selectedUser = matches[indexPath.row]
        self.present(controller, animated: true, completion: nil)
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func downloadMatches(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let users = users else { return }
    
        let databaseNode = FirebaseDataStructure.Users.databaseName
        let matchNode = FirebaseDataStructure.Users.Matches.databaseName
        
        ref.child(databaseNode).child(currentUser.uid).child(matchNode).observeSingleEvent(of: .value, with: { (snapshot) in
            self.matches.removeAll()
            let enumerator = snapshot.children
            
            while let child = enumerator.nextObject() as? DataSnapshot {
                guard let uid = child.value as? String else { continue } // uid of the user who matched with the current user
                
                for user in users {
                    if user.uid == uid {
                        self.matches.append(user)
                    }
                }
            }
            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
