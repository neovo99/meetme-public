//
//  SetUpProfilePictureScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 28..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class SetUpProfilePictureScreenX: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var imgView: UIImageView!
    
    var storageRef:StorageReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initStorageReference()
    }

    @IBAction func buttonOpenLibraryTouched(_ sender: Any) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func buttonNextTouched(_ sender: Any) {
        guard let img = imgView.image else {
            return
        }
        uploadProfilePictureToFirebase(image: img)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let theInfo = info as NSDictionary
        let img = theInfo.object(forKey: UIImagePickerControllerOriginalImage) as! UIImage
        
        imgView.image = img
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func uploadProfilePictureToFirebase(image: UIImage) {
        guard let user = Auth.auth().currentUser else {
            return
        }
        let userImageRef = storageRef.child("\(FirebaseDataStructure.UserImages.databaseName)").child(user.uid).child(FirebaseDataStructure.UserImages.profilepicture)
        
        if let imageData = UIImageJPEGRepresentation(image, 0.5) {
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            let uploadTask = userImageRef.putData(imageData, metadata: metadata, completion: { (metadata, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            })
            uploadTask.observe(.progress) { (snapshot) in
                guard let progress = snapshot.progress else {
                    return
                }
                if progress.completedUnitCount == progress.totalUnitCount {
                }
            }
            let storyboard = UIStoryboard(name: Meetme.Storyboards.SetUpProfileScreen.name, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.SetUpProfileScreen.ViewControllers.SetUpProfileDiscoverySettingsScreenVC) as! SetUpDiscoveryParameterScreenX
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
