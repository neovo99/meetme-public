//
//  SetUpProfileScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 26..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class SetUpProfileScreenX: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldAboutMe: UITextField!
    @IBOutlet weak var textFieldUniversity: UITextField!
    @IBOutlet weak var textFieldJobTitle: UITextField!
    @IBOutlet weak var labelDisplayedName: UILabel!
    @IBOutlet weak var pickerViewGender: UIPickerView!
    @IBOutlet weak var pickerViewYearOfBirth: UIPickerView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let pickerGenderData = ["Man", "Woman"]
    var pickerYearOfBirthData:[String] = [String]()
    
    var ref: DatabaseReference!
    var storageRef:StorageReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        pickerViewYearOfBirth.delegate = self
        pickerViewYearOfBirth.dataSource = self

        pickerViewGender.delegate = self
        pickerViewGender.dataSource = self
        
        textFieldName.delegate = self
        textFieldAboutMe.delegate = self
        textFieldUniversity.delegate = self
        textFieldJobTitle.delegate = self
        
        initDatabaseReference()
        initStorageReference()
        
        fillYearOfBirthPickerViewWithData()
        
        setupTextFieldStyle()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0...2: break
        default:
            scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            textFieldAboutMe.becomeFirstResponder()
        }
        if textField.tag == 1 {
            textFieldUniversity.becomeFirstResponder()
        }
        if textField.tag == 2 {
            textFieldJobTitle.becomeFirstResponder()
        }
        if textField.tag == 3 {
            self.view.endEditing(true)

        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count:Int?
        
        if pickerView == self.pickerViewYearOfBirth {
            count = pickerYearOfBirthData.count
        }
        if pickerView == self.pickerViewGender {
            count = pickerGenderData.count
        }
        return count!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var text:String?
        
        if pickerView == self.pickerViewYearOfBirth {
            text = pickerYearOfBirthData[row]
        }
        if pickerView == self.pickerViewGender {
            text = pickerGenderData[row]
        }
        return text!
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var title: NSAttributedString?
        
        if pickerView == self.pickerViewYearOfBirth {
            let titleData = pickerYearOfBirthData[row]
            title = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if pickerView == self.pickerViewGender {
            let titleData = pickerGenderData[row]
            title = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        return title
    }

    @IBAction func buttonNextTouched(_ sender: Any) {
        if !isFormValid() {
            return
        }
        writeDataToFirebase()
        let storyboard = UIStoryboard(name: Meetme.Storyboards.SetUpProfileScreen.name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.SetUpProfileScreen.ViewControllers.SetUpProfilePictureScreenVC) as! SetUpProfilePictureScreenX
        downloadProfilePictureFromFirebase { (snapshot) in
            controller.imgView.image = snapshot
        }
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func initStorageReference() {
        storageRef = Storage.storage().reference()
    }
    
    func setupTextFieldStyle() {
        textFieldName.layer.borderWidth = 1.0
        textFieldUniversity.layer.borderWidth = 1.0
        textFieldJobTitle.layer.borderWidth = 1.0
        textFieldAboutMe.layer.borderWidth = 1.0
        textFieldName.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
        textFieldUniversity.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
        textFieldJobTitle.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
        textFieldAboutMe.layer.borderColor = Meetme.Colors.textFieldBorder.cgColor
    }
    
    func isFormValid() -> Bool {
        guard let text = textFieldName.text else {
            labelDisplayedName.textColor = UIColor.red
            return false
        }
        if text == "" {
            labelDisplayedName.textColor = UIColor.red
            return false
        }
        return true
    }
    
    func writeDataToFirebase() {
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(user.uid)
        userNode.child(FirebaseDataStructure.Users.email).setValue(user.email)
        userNode.child(FirebaseDataStructure.Users.username).setValue(textFieldName.text)
        userNode.child(FirebaseDataStructure.Users.bio).setValue(textFieldAboutMe.text)
        userNode.child(FirebaseDataStructure.Users.universityName).setValue(textFieldUniversity.text)
        userNode.child(FirebaseDataStructure.Users.jobTitle).setValue(textFieldJobTitle.text)
        let age = Int(pickerYearOfBirthData[pickerViewYearOfBirth.selectedRow(inComponent: 0)])
        userNode.child(FirebaseDataStructure.Users.age).setValue(age)
        userNode.child(FirebaseDataStructure.Users.gender).setValue(pickerGenderData[pickerViewGender.selectedRow(inComponent: 0)])
    }
    
    func fillYearOfBirthPickerViewWithData() {
        let minAge = 18
        let maxAge = 99
        
        for age in minAge...maxAge {
            pickerYearOfBirthData.append(String(age))
        }
    }
    
    func downloadProfilePictureFromFirebase(completion: @escaping (_ snapshot :UIImage) -> Void) {
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        let userImageRef = storageRef.child(FirebaseDataStructure.UserImages.databaseName).child(user.uid).child(FirebaseDataStructure.UserImages.profilepicture)
        
        userImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let img = UIImage(data: data!) {
                    completion(img)
                }
            }
        }
    }
}
