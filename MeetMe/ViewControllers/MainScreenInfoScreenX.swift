//
//  MainScreenInfoScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 29..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import SafariServices

class MainScreenInfoScreenX: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var tableData = ["Firebase", "Google Maps", "WARangeSlider", "UICircularProgressRing", "PopupDialog", "MessageKit", "LGButton", "LIHImageSlider", "PageMenu"]
    var urls = ["https://firebase.google.com", "https://cloud.google.com/maps-platform",
                "https://github.com/warchimede/RangeSlider", "ttps://github.com/luispadron/UICircularProgressRing",
                "https://github.com/Orderella/PopupDialog", "https://github.com/MessageKit/MessageKit",
                "https://github.com/loregr/LGButton", "https://github.com/Lasithih/LIHImageSlider",
                "https://github.com/PageMenu/PageMenu"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell!.textLabel?.text = tableData[indexPath.row]
        cell!.textLabel?.textColor = UIColor.white
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = Meetme.Colors.tableViewCellSelected
        cell!.selectedBackgroundView = backgroundView
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = tableData[indexPath.row]
        
        switch title {
        case "Firebase":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "Google Maps":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "WARangeSlider":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "UICircularProgressRing":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "PopupDialog":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "MessageKit":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "LGButton":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "LIHImageSlider":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        case "PageMenu":
            guard let url = URL(string: urls[indexPath.row]) else { return }
            let webViewController = SFSafariViewController(url: url)
            webViewController.preferredBarTintColor = UIColor.black
            present(webViewController, animated: true, completion: nil)
        default:
            print("Error")
        }
    }
    
    @IBAction func buttonBackTouched(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
