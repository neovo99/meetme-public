//
//  SignUpScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 24..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class SignUpScreenX: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldPassword2: UITextField!
    @IBOutlet weak var labelErrorMessage: UILabel!
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewBackground.blurImage(intensity: 5.0)
        
        labelErrorMessage.text = ""
        
        textFieldEmail.delegate = self
        textFieldPassword.delegate = self
        textFieldPassword2.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0...1: break
        default:
            scrollView.setContentOffset(CGPoint(x: 0,y: 100), animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            textFieldPassword.becomeFirstResponder()
        }
        if textField.tag == 1 {
            textFieldPassword2.becomeFirstResponder()
        }
        if textField.tag == 2 {
            self.view.endEditing(true)
        }
        return true
    }
    
    @IBAction func buttonBackTouched(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSignUpTouched(_ sender: Any) {
        signUpUser()
    }
    
    func signUpUser() {
        guard let email = textFieldEmail.text else { return }
        guard let password = textFieldPassword.text else { return }
        guard let password2 = textFieldPassword2.text else { return }
        
        if email == "" {
            labelErrorMessage.text = "The email address is empty"
            return
        }
        if !email.isValidEmail() {
            labelErrorMessage.text = "The email address is malformed"
            return
        }
        if password.count < 6 {
            labelErrorMessage.text = "The password must be 6 characters long or more"
            return
        }
        if password != password2 {
            labelErrorMessage.text = "Passwords are not matching"
            return
        }

        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            var message = ""
            
            if let error = error {
                message = error.localizedDescription
                
                let alert = UIAlertController(title: "Error occured", message: message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            message = "Now you can log in!"
            
            let alert = UIAlertController(title: "Successful Registration", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                self.navigationController!.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
