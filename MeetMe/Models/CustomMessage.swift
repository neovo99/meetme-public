//
//  CustomMessage.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 22..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import CoreLocation
import MessageKit

public enum MessageKind {
    case text(String)
    case attributedText(NSAttributedString)
    case emoji(String)
    case custom(Any?)
}

struct CustomMessage: MessageType {
    var data: MessageData
    var messageId: String
    var sender: Sender
    var sentDate: Date
    var kind: MessageKind
    
    private init(kind: MessageKind, sender: Sender, messageId: String, date: Date, data: MessageData) {
        self.kind = kind
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
        self.data = data
    }
    
    init(text: String, sender: Sender, messageId: String, date: Date, data: MessageData) {
        self.init(kind: .text(text), sender: sender, messageId: messageId, date: date, data: data)
    }
    
    init(attributedText: NSAttributedString, sender: Sender, messageId: String, date: Date, data: MessageData) {
        self.init(kind: .attributedText(attributedText), sender: sender, messageId: messageId, date: date, data: data)
    }
    
    init(emoji: String, sender: Sender, messageId: String, date: Date, data: MessageData) {
        self.init(kind: .emoji(emoji), sender: sender, messageId: messageId, date: date, data: data)
    }
    
}
