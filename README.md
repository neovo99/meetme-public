# README.md
### MeetMe Dating Application (Swift 4)

---

To use it, simply:

* **Run** in project folder
```objective-c
pod install
```

* Add your **Google Maps API key** in AppleDelegate.swift at didFinishLaunchingWithOptions
```objective-c
GMSServices.provideAPIKey("YOUR_API_KEY_GOES_HERE")
```
* Copy your own **GoogleService-Info.plist** to the project.

---

**Under the hood**

* Authentication: Firebase Authentication
* Database: Firebase Realtime Database and Firebase Storage
* Location: iOS CoreLocation and Google Maps SDK
* Communication: MessageKit

---

**Used Libraries**

* [Firebase](https://firebase.google.com "Title")
* [Google Maps]( https://cloud.google.com/maps-platform "Title")
* [WARangeSlider]( https://github.com/warchimede/RangeSlider "Title")
* [UICircularProgressRing]( https://github.com/luispadron/UICircularProgressRing "Title")
* [PopupDialog]( https://github.com/Orderella/PopupDialog "Title")
* [MessageKit]( https://github.com/MessageKit/MessageKit "Title")
* [LGButton]( https://github.com/loregr/LGButton "Title")
* [LIHImageSlider]( https://github.com/Lasithih/LIHImageSlider "Title")
* [PageMenu]( "https://github.com/PageMenu/PageMenu" "Title")

---

**You can see the application running below**

[Youtube - MeetMe Preview - (Swift) 2018 - Borsos Balázs](https://www.youtube.com/watch?v=vKFOVU2XgVo "Title")

---

**Contact**

Balázs Borsos - borsosbal@gmail.com