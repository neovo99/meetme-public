//
//  SetUpDiscoveryParameterScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 28..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class SetUpDiscoveryParameterScreenX: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var labelRange: UILabel!
    @IBOutlet weak var pickerViewGender: UIPickerView!
    
    let pickerViewGenderData = ["Man", "Woman", "Man and Woman"]
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        pickerViewGender.delegate = self
        pickerViewGender.dataSource = self
        
        initDatabaseReference()
        
        initRangeSlider(rangeSlider: rangeSlider)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadDataFromFirebase()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewGenderData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewGenderData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        writeDataToFirebase()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var title: NSAttributedString?
            let titleData = pickerViewGenderData[row]
            title = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        return title
    }
    
    @IBAction func buttonNextTouched(_ sender: Any) {
        writeDataToFirebase()
        transitToController(storyboardName: Meetme.Storyboards.LoginScreen.name, viewControllerName: Meetme.Storyboards.LoginScreen.entryNC, completion: {})
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        let minAge = Int(rangeSlider.lowerValue)
        let maxAge = Int(rangeSlider.upperValue)
        if maxAge < 55 {
         labelRange.text = ("\(minAge)-\(maxAge)")
        } else {
         labelRange.text = ("\(minAge)-\(maxAge)+")
        }
        writeDataToFirebase()
    }
    
    func initRangeSlider(rangeSlider: RangeSlider) {
        rangeSlider.maximumValue = 55
        rangeSlider.minimumValue = 18
        rangeSlider.upperValue = 55
        rangeSlider.lowerValue = 18
        rangeSlider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
    }
    
    func writeDataToFirebase() {
        guard let user = Auth.auth().currentUser else { return }
        
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(user.uid)
        
        let minAge = Int(rangeSlider.lowerValue)
        let maxAge = Int(rangeSlider.upperValue)
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.minAge).setValue(minAge)
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.maxAge).setValue(maxAge)
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.gender).setValue(pickerViewGenderData[pickerViewGender.selectedRow(inComponent: 0)])
    }
    
    func loadDataFromFirebase() {
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(user.uid).child(FirebaseDataStructure.Users.Discovery.databaseName).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else {
                return
            }
            
            guard let gender = value[FirebaseDataStructure.Users.Discovery.gender] as? String else { return }
            guard let minAge = value[FirebaseDataStructure.Users.Discovery.minAge] as? Int else { return }
            guard let maxAge = value[FirebaseDataStructure.Users.Discovery.maxAge] as? Int else { return }
            
            self.rangeSlider.lowerValue = Double(minAge)
            self.rangeSlider.upperValue = Double(maxAge)
            
            switch gender {
            case self.pickerViewGenderData[0]:
                self.pickerViewGender.selectRow(0, inComponent: 0, animated: false)
                break
            case self.pickerViewGenderData[1]:
                self.pickerViewGender.selectRow(1, inComponent: 0, animated: false)
                break
            case self.pickerViewGenderData[2]:
                self.pickerViewGender.selectRow(2, inComponent: 0, animated: false)
                break
            default:
                self.pickerViewGender.selectRow(0, inComponent: 0, animated: false)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
