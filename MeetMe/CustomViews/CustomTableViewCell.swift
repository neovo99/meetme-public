//
//  CustomTableViewCell.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 20..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    
    func setupCell(image: UIImage?, labelText: String) {
        if let image = image {
            self.imgView.image = image
        }
        self.labelUserName.text = labelText
    }
}
