//
//  ApplicationStructure.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 11..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit

struct Meetme {
    struct Storyboards {
        struct LoginScreen {
            static let name = "LoginScreen"
            static let entryNC = "LoginNavigationController"
            struct ViewControllers {
                static let LoginScreen = "LoginViewController"
                static let SignUpScreen = "SignUpViewController"
            }
        }
        
        struct SetUpProfileScreen {
            static let name = "SetUpProfileScreen"
            static let entryNC = "SetUpProfileNavigationController"
            struct ViewControllers {
                static let SetUpProfileScreenVC = "SetUpProfileViewController"
                static let SetUpProfilePictureScreenVC = "SetUpProfilePictureViewController"
                static let SetUpProfileDiscoverySettingsScreenVC = "DiscoverySettingsViewController"
                static let SetUpProfileWelcomScreenVC = "SetUpProfileWelcomViewController"
            }
        }
        
        struct MainScreen {
            static let name = "MainScreen"
            struct ViewControllers {
                static let entryNC = "MainNavigationController"
                static let mainScreenVC = "MainViewController"
                struct ViewControllers {
                    struct MainProfileScreen {
                        static let entryNC = "MainProfileNavigationController"
                        static let mainProfileScreenVC = "MainProfileViewController"
                        struct ViewControllers {
                            static let mainProfileScreenDiscoverySettingsVC = "MainProfileDiscoverySettingsViewController"
                            static let mainProfileScreenEditInfoVC = "MainProfileScreenEditInfoViewController"
                            static let MainProfileScreenApplicationInfoVC = "MainProfileScreenApplicationInfoViewController"
                        }
                    }
                    struct MainMapScreen {
                        static let mainMapScreenVC = "MainMapViewController"
                    }
                    struct MainDiscoveryScreen {
                        static let entryVC = "MainDiscoveryNavigationController"
                        static let mainDiscoveryScreenVC = "MainDiscoveryViewController"
                    }
                }
            }
        }
        
        struct UserProfileScreen {
            static let name = "UserProfileScreen"
            static let userProfileScreenVC = "UserProfileViewController"
        }
        
        struct LoadingScreen {
            static let name = "LoadingScreen"
            struct ViewControllers {
                static let entry = "LoadingScreenViewController"
            }
        }
        
        struct LaunchScreen {
            static let name = "LaunchScreen"
            struct ViewControllers {
                static let entry = "Launch"
            }
        }
    }
    
    struct Colors {
        static let moderateCyan = UIColor.init(netHex: 0x46B4AA) // moderate cyan
        static let darkGrey = UIColor.init(netHex: 0x2F2F2F) // dark-grey
        
        static let currentUserImageBorder = moderateCyan
        
        static let mapMarkerCurrentUser = moderateCyan
        static let mapMarkerUser = UIColor.black
        
        static let backgroundColor = darkGrey
        static let textFieldBorder = UIColor.white
        
        static let menuBarBackground = UIColor.black
        static let menuBarIndicator = moderateCyan
        static let menuBarHighlightedText = moderateCyan
        static let menuBarUnselectedText = UIColor.white
        
        static let tableViewCellSelected = moderateCyan
        
        static let chatHeader = UIColor.black
        static let chatBackground = darkGrey
        static let chatHeadBorder = moderateCyan
        static let chatHeaderText = UIColor.white
        static let chatBubbleCurrentUser = moderateCyan
        static let chatDateIndicatorText = UIColor.white
        static let chatInputBarBackground = UIColor.black
        static let chatInputBar = darkGrey
        static let chatInputBarPlaceholder = moderateCyan
        static let chatInputBarText = UIColor.white
    }
}

