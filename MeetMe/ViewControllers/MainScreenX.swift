//
//  MainScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 04..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

// PageMenu Github: https://github.com/PageMenu/PageMenu

import UIKit
import Firebase

class MainScreenX: UIViewController, CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var user:User?
    var users:[User]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        var controllerArray : [UIViewController] = []
        
        let storyboard = UIStoryboard(name: Meetme.Storyboards.MainScreen.name, bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainProfileScreen.entryNC) as! UINavigationController
        let mapVC = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainMapScreen.mainMapScreenVC) as! MainScreenMapX
        let discoveryVC = storyboard.instantiateViewController(withIdentifier: Meetme.Storyboards.MainScreen.ViewControllers.ViewControllers.MainDiscoveryScreen.entryVC) as! UINavigationController
        
        let mainProfileScreen = profileVC.viewControllers.first as! MainScreenProfileX
        mainProfileScreen.user = user
        
        mapVC.user = user
        mapVC.users = users
        
        let mainDiscoveryScreen = discoveryVC.viewControllers.first as! MainScreenDiscoveryX
        mainDiscoveryScreen.user = user
        mainDiscoveryScreen.users = users
        
        profileVC.title = "Profile"
        mapVC.title = "Map"
        discoveryVC.title = "Discovery"
        
        controllerArray.append(profileVC)
        controllerArray.append(mapVC)
        controllerArray.append(discoveryVC)
        
        let backgroundColor = UIColor.white
        //let backgroundColor = Meetme.Colors.backgroundColor
        let scrollMenuBackgroundColor = Meetme.Colors.menuBarBackground
        let bottomMenuHairlineColor = UIColor.clear
        let selectedMenuItemLabelColor = Meetme.Colors.menuBarHighlightedText
        let unselectedMenuItemLabelColor = Meetme.Colors.menuBarUnselectedText
        let selectionIndicatorColor = Meetme.Colors.menuBarIndicator
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .viewBackgroundColor(backgroundColor),
            .scrollMenuBackgroundColor(scrollMenuBackgroundColor),
            .bottomMenuHairlineColor (bottomMenuHairlineColor),
            .selectedMenuItemLabelColor(selectedMenuItemLabelColor),
            .unselectedMenuItemLabelColor(unselectedMenuItemLabelColor),
            .selectionIndicatorColor(selectionIndicatorColor),
            .menuItemSeparatorWidth(CGFloat(0.0)),
            .enableHorizontalBounce(false)
        ]

            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0,y: UIApplication.shared.statusBarFrame.height, width: self.view.frame.width,height: self.view.frame.height), pageMenuOptions: parameters)
        
        pageMenu!.delegate = self
        
        addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {}
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {}
}
