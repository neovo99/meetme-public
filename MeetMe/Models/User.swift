//
//  User.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 05. 26..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class User {
    let uid:String
    let username:String
    var bio:String? // bio
    var universityName:String?
    var jobTitle:String?
    let age:Int
    let gender:String
    var discoverySettings:DiscoverySettings?
    var location:Location?
    var profileImage:UIImage?
    var images:[UIImage?]?
    
    init(uid:String, username:String, bio:String, universityName:String, jobTitle:String, age:Int, gender:String, discoverySettings:DiscoverySettings, location:Location, profileImage:UIImage?, images:[UIImage]?) {
        self.uid = uid
        self.username = username
        self.bio = bio
        self.universityName = universityName
        self.jobTitle = jobTitle
        self.age = age
        self.gender = gender
        self.discoverySettings = discoverySettings
        self.location = location
        self.profileImage = profileImage
        self.images = images
    }
}

class DiscoverySettings {
    var minAge:Int
    var maxAge:Int
    var gender:String
    
    init(minAge:Int, maxAge:Int, gender:String) {
        self.minAge = minAge
        self.maxAge = maxAge
        self.gender = gender
    }
}

class Location {
    var long:CLLocationDegrees
    var lat:CLLocationDegrees
    
    init(long:CLLocationDegrees,lat:CLLocationDegrees) {
        self.long = long
        self.lat = lat
    }
}
