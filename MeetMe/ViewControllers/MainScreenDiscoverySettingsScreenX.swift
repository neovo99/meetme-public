//
//  MainScreenDiscoverySettingsScreenX.swift
//  MeetMe
//
//  Created by Borsos Balázs on 2018. 06. 05..
//  Copyright © 2018. Borsos Balázs. All rights reserved.
//

import UIKit
import Firebase

class MainScreenDiscoverySettingsScreenX: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, FirebaseCurrentUserManager {
    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var pickerViewGender: UIPickerView!
    @IBOutlet weak var labelRange: UILabel!
    
    let pickerViewGenderData = ["Man", "Woman", "Man and Woman"]
    var ref: DatabaseReference!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        pickerViewGender.delegate = self
        pickerViewGender.dataSource = self
        
        initDatabaseReference()
    
        initRangeSlider(rangeSlider: rangeSlider)
        
        if user == nil {
            downloadUserData(completion: {})
        } else {
            setupView(with: user!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if user == nil {
            downloadUserData(completion: {})
        } else {
            setupView(with: user!)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewGenderData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewGenderData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var title: NSAttributedString?
        let titleData = pickerViewGenderData[row]
        title = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        return title
    }
    
    func initDatabaseReference() {
        ref = Database.database().reference()
    }
    
    func downloadUserData(completion: @escaping () -> Void) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let discoveryData = snapshot.childSnapshot(forPath: FirebaseDataStructure.Users.Discovery.databaseName)
            guard let discoveryValue = discoveryData.value as? NSDictionary else { return }
            
            let minAge = discoveryValue[FirebaseDataStructure.Users.Discovery.minAge] as! Int
            let maxAge = discoveryValue[FirebaseDataStructure.Users.Discovery.maxAge] as! Int
            let discoveryGender = discoveryValue[FirebaseDataStructure.Users.Discovery.gender] as! String
            
            if maxAge < 55 {
                self.labelRange.text = "\(minAge)-\(maxAge)"
            } else {
                self.labelRange.text = "\(minAge)-\(maxAge)+"
            }
            
            self.rangeSlider.lowerValue = Double(minAge)
            self.rangeSlider.upperValue = Double(maxAge)
            if let row = self.pickerViewGenderData.index(of: discoveryGender) {
                self.pickerViewGender.selectRow(row, inComponent: 0, animated: false)
            }
            completion()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func uploadUserData(user: User) {
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        let userNode = ref.child(FirebaseDataStructure.Users.databaseName).child(currentUser.uid)

        let minAge = Int(rangeSlider.lowerValue)
        let maxAge = Int(rangeSlider.upperValue)
        let discoveryGender = pickerViewGenderData[pickerViewGender.selectedRow(inComponent: 0)]
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.minAge).setValue(minAge)
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.maxAge).setValue(maxAge)
        userNode.child(FirebaseDataStructure.Users.Discovery.databaseName).child(FirebaseDataStructure.Users.Discovery.gender).setValue(discoveryGender)
    }
    
    @IBAction func buttonBackTouched(_ sender: Any) {
        if let user = user {
            uploadUserData(user: user)
            user.discoverySettings?.minAge = Int(rangeSlider.lowerValue)
            user.discoverySettings?.maxAge = Int(rangeSlider.upperValue)
            user.discoverySettings?.gender = pickerViewGenderData[pickerViewGender.selectedRow(inComponent: 0)]
        } else {
            print("Error @ MainDiscoverySettings: Cannot upload user data because it's nil")
        }
        let controller = navigationController?.previousViewController() as! MainScreenProfileX
        controller.user = user
        navigationController?.popToViewController(controller, animated: true)
    }
    
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        let minAge = Int(rangeSlider.lowerValue)
        let maxAge = Int(rangeSlider.upperValue)
        if maxAge < 55 {
            labelRange.text = ("\(minAge)-\(maxAge)")
        } else {
            labelRange.text = ("\(minAge)-\(maxAge)+")
        }
    }
    
    func setupView(with user: User) {
        guard let discoverySettings = user.discoverySettings else { return }
        rangeSlider.lowerValue = Double(discoverySettings.minAge)
        rangeSlider.upperValue = Double(discoverySettings.maxAge)
        if discoverySettings.maxAge < 55 {
            self.labelRange.text = "\(discoverySettings.minAge)-\(discoverySettings.maxAge)"
        } else {
            self.labelRange.text = "\(discoverySettings.minAge)-\(discoverySettings.maxAge)+"
        }
        if let row = self.pickerViewGenderData.index(of: discoverySettings.gender) {
            self.pickerViewGender.selectRow(row, inComponent: 0, animated: false)
        }
    }
    
    func initRangeSlider(rangeSlider: RangeSlider) {
        rangeSlider.maximumValue = 55
        rangeSlider.minimumValue = 18
        rangeSlider.upperValue = 55
        rangeSlider.lowerValue = 18
        rangeSlider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
    }
}
